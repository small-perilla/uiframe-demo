using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PanSkill : UIBase
{
    PanSkill()
    {
        uiLayer = UILayer.Default;
        uiType = UIType.UISkill;
    }

    public Button btnClose;

    public void Start()
    {
        btnClose.onClick.AddListener(() => { UIManager.I.PopPanel(); });
    }

    public override void OnEnter()
    {
        base.OnEnter();
        this.gameObject.SetActive(true);
    }

    public override void OnPause()
    {
        //btnClose.enabled = false;
    }

    public override void OnResume()
    {
        //btnClose.enabled = true;
    }

    public override void OnExit()
    {
        this.gameObject.SetActive(false);
    }
}
