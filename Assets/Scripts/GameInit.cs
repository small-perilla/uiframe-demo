using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameInit : MonoSingle<GameInit>
{
    private void Awake()
    {
        DontDestroyOnLoad(this.gameObject);
        UIManager.I.PushPanel(UIType.UIMain);
    }
}
