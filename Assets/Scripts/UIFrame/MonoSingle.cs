using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MonoSingle<T> : MonoBehaviour where T :MonoSingle<T>
{
    protected static T instance;

    public static T I
    {
        get
        {
            if (instance == null)
            {
                GameObject go = GameObject.Find("GameInit");
                if (go == null)
                {
                    go = new GameObject("GameInit");
                    DontDestroyOnLoad(go);
                }

                instance = go.GetComponent<T>();

                if (instance == null)
                    instance = go.AddComponent<T>();
            }

            return instance;
        }
    }
}
