using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public partial class UIManager : MonoSingle<UIManager>
{
    private void InitPath()
    {
        dicPath = new Dictionary<string, string>();
        //自动生成代码
        dicPath["panmain"] = "UIPanel/PanMain";
        dicPath["paninventory"] = "UIPanel/paninventory";
        dicPath["panskill"] = "UIPanel/panskill";
        dicPath["panquest"] = "UIPanel/panquest";
        dicPath["panequipment"] = "UIPanel/panequipment";
        dicPath["panshop"] = "UIPanel/panshop";
    }

    private void InitUILayer()
    {
        dicLayerName = new Dictionary<UILayer, string>();
        dicLayerName.Add(UILayer.Back,"Front");
        dicLayerName.Add(UILayer.Default,"Default");
        dicLayerName.Add(UILayer.Pop,"Pop");
        dicLayerName.Add(UILayer.Top,"Top");
    }
}