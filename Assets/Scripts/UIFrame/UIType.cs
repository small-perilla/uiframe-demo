using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIType
{
    public const string UIMain = "panmain";
    public const string UIInventory = "paninventory";
    public const string UIShop = "panshop";
    public const string UIQuest = "panquest";
    public const string UIEquipment = "panequipment";
    public const string UISkill = "panskill";
}

public enum UILayer
{
    Back,      //背景层
    Default,   //默认层
    Pop,       //弹窗
    Top        //顶层，适用悬浮等
}

