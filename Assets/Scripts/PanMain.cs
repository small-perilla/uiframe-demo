using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

public class PanMain : UIBase
{
    PanMain()
    {
        //初始化UIBase中层级和类型字段
        uiLayer = UILayer.Default;
        uiType = UIType.UIMain;
    }

    //打开界面按钮，公有字段，inspector界面赋值
    public Button btnInventory;
    public Button btnShop;
    public Button btnSkill;
    public Button btnQuest;
    public Button btnEquipment;

    public override void OnEnter()
    {
        base.OnEnter();
        Debug.Log("打开Mian");
        
        //封装的按钮添加事件方法，点击打开或关闭
        AddBtnListener(btnEquipment,UIType.UIEquipment);
        AddBtnListener(btnShop,UIType.UIShop);
        AddBtnListener(btnSkill,UIType.UISkill);
        AddBtnListener(btnQuest,UIType.UIQuest);
        AddBtnListener(btnInventory,UIType.UIInventory);
    }

    private void AddBtnListener(Button go,string type)
    {
        go.onClick.AddListener(() =>
        {
            if (UIManager.I.GetTopPanel().uiType == type)
                UIManager.I.PopPanel();
            else
                UIManager.I.PushPanel(type);
        });
    }

    public override void OnPause()
    {
        //取消下面注释，再打开界面时，主界面按钮会失效
        //ChangeBtnState(false);
    }

    public override void OnResume()
    {
        //ChangeBtnState(true);
    }

    public override void OnExit()
    {
    }

    //按钮失效代码
    public void ChangeBtnState(bool value)
    {
        btnInventory.enabled = value;
        btnShop.enabled = value;
        btnSkill.enabled = value;
        btnQuest.enabled = value;
        btnEquipment.enabled = value;
    }
}
